
import 'main.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

const viruskey = '69da6de595b8a8d67a65c3b84dc768a2ab5f634982091b9fddc15765a519fbbd';
const voidkey = '5fe37ea1c04734b61814b33590b4cefc87971ecf';
const googlekey = 'AIzaSyBYwcQYGzJcn-kW2_e2M4BoNzMFtsG46dw';

class URLDetails extends StatefulWidget 
{
  const URLDetails({super.key, required this.virusData, required this.apivoidData, required this.googleData});

  final Future<Map<String,dynamic>> virusData;
  final Future<Map<String,dynamic>> apivoidData;
  final Future<Map<String,dynamic>> googleData;

  @override
  State<URLDetails> createState() => _URLDetailsState();
}

class _URLDetailsState extends State<URLDetails> 
{

  @override
  void initState()
  {
    super.initState();
  }


  String _selectedItem = 'VirusTotal';
  @override
  Widget build(BuildContext context) 
  {
    Widget body;
    if (_selectedItem == 'VirusTotal') 
    {
      body = VirusDetails(data: widget.virusData);
    } 
    else if (_selectedItem == 'APIVoid')
    {
      body = APIVoidDetails(data: widget.apivoidData);
    }
    else
    {
      body = GoogleDetails(data: widget.googleData);
    }
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        title: const Text('URL Details', style: TextStyle(color: Colors.black)),
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        actions: <Widget>[
          Row(
            children: <Widget>[

          DropdownButton<String>(
            borderRadius: BorderRadius.circular(10),
            icon: const Icon(Icons.arrow_drop_down, color: Colors.black),
            style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0), fontSize: 16,),
            iconSize: 40,
            value: _selectedItem,
            items: <String>['VirusTotal', 'APIVoid', 'Google Safe Browsing'].map((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
              onChanged: (value) {
                setState(() {
                  _selectedItem = value!;
                });

              },
          ),
          const SizedBox(width: 10,),
            ]
          )
            ],
          ),
      body: body

    );
  }
}

class VirusDetails extends StatelessWidget 
{
  const VirusDetails({Key? key, required this.data}) : super(key: key);

  final Future<Map<String, dynamic>> data;

  @override
  Widget build(BuildContext context) 
  {
    return CustGradient(
      child:
    FutureBuilder<Map<String, dynamic>>(
      future: data,
      builder: (context, snapshot) 
      {
        if (snapshot.connectionState == ConnectionState.waiting) 
        {
          return const Center(child: CircularProgressIndicator());
        } 
        else if (snapshot.hasError) 
        {
          return Center(child: Text('Error: ${snapshot.error}'));
        } 
        else 
        {
          final results = snapshot.data!['data']['attributes']['results'];
           var filteredEntries = results.entries.where((entry) =>
          entry.value['result'] == 'phishing' ||
          entry.value['result'] == 'malicious').toList();
    
          if (filteredEntries.isEmpty) 
          {
            return const Center(
          child: Text('Url was not flagged as suspicious in any checks', style: TextStyle(color: Colors.black)),
            );
            }
          return ListView(
            children: filteredEntries
            .map<Widget>((entry) {
                var record = entry.value as Map<String, dynamic>;
                return 
                  ListTile(
                  textColor: Colors.black,
                  title: Text(entry.key, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  subtitle: 
                  Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white
                  ),
                  child:
                  Column(
                    children: [
                    Text('Method: ${record['method'] ?? 'N/A'}, '),
                    Text('Engine Name: ${record['engine_name'] ?? 'N/A'}, '),
                    Text('Category: ${record['category'] ?? 'N/A'}, '),
                    Text('Result: ${record['result'] ?? 'N/A'}'),
                    ]
                  ),
                )
                );
              }).toList(),
          );
        }
      },
    )
    );
  }
}


class APIVoidDetails extends StatelessWidget 
{
  const APIVoidDetails({Key? key, required this.data}) : super(key: key);

  final Future<Map<String, dynamic>> data;

  @override
  Widget build(BuildContext context) 
  {
    return CustGradient(
      child:
    FutureBuilder<Map<String, dynamic>>(
      future: data,
      builder: (context, snapshot) 
      {
        if (snapshot.connectionState == ConnectionState.waiting) 
        {
          return const Center(child: CircularProgressIndicator());
        } 
        else if (snapshot.hasError) 
        {
          return Center(child: Text('Error: ${snapshot.error}'));
        } 
        else 
        {
          final nsRecords = snapshot.data!['data']['report']['dns_records']['ns']['records'];
          final mxRecords = snapshot.data!['data']['report']['dns_records']['mx']['records'];
          final domainBlacklist = snapshot.data!['data']['report']['domain_blacklist']['engines'];
          final securityChecks = snapshot.data!['data']['report']['security_checks'];
          final riskScore = snapshot.data!['data']['report']['risk_score']['result'];
              return ListView(
                children: [
                  ListTile(
                    textColor: Colors.black,
                    title: const Text('Risk Score', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                    subtitle: 
                    Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white
                  ),
                  child: Text('$riskScore\n'),
                  ),
                  ),
                  ListTile(
            title: const Text('NS Records', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
            subtitle: 
            Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white
                  ),
                  child:
                  Column(
                    children: [
          ...nsRecords.map<Widget>((record) {
            return ListTile(
              textColor: Colors.black,
              title: Text(record['target']),
              subtitle: Text('IP: ${record['ip']}, Country: ${record['country_name']}, ISP: ${record['isp']}'),
            );
          }).toList(),
                    ]
                  )
            )
                  ),
          ListTile(
            title: const Text('MX Records', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
            subtitle:
           Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white
                  ),
                  child:
                  Column(
                    children: [
          ...mxRecords.map<Widget>((record) {
            return ListTile(
              textColor: Colors.black,
              title: Text(record['target']),
              subtitle: Text('IP: ${record['ip']}, Country: ${record['country_name']}, ISP: ${record['isp']}'),
            );
          }).toList(),
              ]
            )
          ),
          ),
          ListTile(

            title: const Text('Domain Blacklist', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
            subtitle: 
          Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white
                  ),
          child:
          Column(
            children: [
          ...domainBlacklist.map<Widget>((engine) {
            if (engine['detected'] == 0) {
              return ListTile(
                textColor: Colors.black,
                title: Text(engine['name']),
                subtitle: const Text('Detected: False'),
              );
            }
            return ListTile(
              textColor: Colors.black,
              title: Text(engine['name']),
              subtitle: const Text('Detected: True'),
              
            );
          }).toList()
            ]
          )
          )
          ),
          ListTile(
            textColor: Colors.black,
            title: const Text('Security Checks', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
            subtitle:
            Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white
                  ),
            child:
              Column(
              children: <Widget>[
                ListTile(
                  textColor: Colors.black,
                  title: const Text('is_most_abused_tld'),
                  subtitle: Text(securityChecks['is_most_abused_tld'].toString()),
                ),
                ListTile(
                  textColor: Colors.black,
                  title: const Text('is_domain_ipv4_assigned'),
                  subtitle: Text(securityChecks['is_domain_ipv4_assigned'].toString()),
                ),
                ListTile(
                  textColor: Colors.black,
                  title: const Text('is_domain_ipv4_private'),
                  subtitle: Text(securityChecks['is_domain_ipv4_private'].toString()),
                ),
                ListTile(
                  textColor: Colors.black,
                  title: const Text('is_domain_blacklisted'),
                  subtitle: Text(securityChecks['is_domain_blacklisted'].toString()),
                ),
                ListTile(
                  textColor: Colors.black,
                  title: const Text('website_popularity'),
                  subtitle: Text(securityChecks['website_popularity'].toString()),
                ),
                ListTile(
                  textColor: Colors.black,
                  title: const Text('is_risky_category'),
                  subtitle: Text(securityChecks['is_risky_category'].toString()),
                ),
                ]
                ),
              ),
          )
            ],
              );
        }
      }
    )
    );
  }
  }

class GoogleDetails extends StatelessWidget
{
  const GoogleDetails({Key? key, required this.data}) : super(key: key);

  final Future<Map<String, dynamic>> data;

  @override
  Widget build(BuildContext context) 
  {
    return CustGradient(
      child:
    FutureBuilder<Map<String, dynamic>>(
      future: data,
      builder: (context, snapshot) 
      {
        if (snapshot.connectionState == ConnectionState.waiting) 
        {
          return const Center(child: CircularProgressIndicator());
        } 
        else if (snapshot.hasError) 
        {
          return Center(child: Text('Error: ${snapshot.error}'));
        } 
        else 
        {
          if (snapshot.data!['matches'] == null) 
          {
            return ListView(
              children: const <Widget>[
                ListTile(
                  textColor: Colors.black,
                  title: Text('Matches'),
                  subtitle: Text('Domain was not found in the Safe Browsing database.'),
                ),
              ],
            );
          }
          return ListView(
            children: <Widget>[
              ListTile(
                textColor: Colors.black,
                title: const Text('Matches'),
                subtitle: Text(snapshot.data!['matches'].toString()),
              ),
            ],
          );
        }
      },
    )
    );
  }
}

String getHost(String value) 
  {
    final Uri? uri = Uri.tryParse(value);
    if (uri != null && uri.hasScheme && uri.hasAuthority) 
    {
      return uri.host;
    }
    return "";
  }

Future<Map<String,dynamic>> getAPIVoidData(url) async
  {
    Uri uri = Uri.parse('https://endpoint.apivoid.com/urlrep/v1/pay-as-you-go/?key=$voidkey&url=$url');
    var voidResponse = await http.get(uri);
    if (voidResponse.statusCode == 200) 
    {
      final Map<String, dynamic> resp = jsonDecode(voidResponse.body);
      return resp;
    } 
    else 
    {
      throw Exception('Failed to load data');
    }
  }

  Future<Map<String,dynamic>> getGoogleData(url) async
  {
    Uri uri = Uri.parse('https://safebrowsing.googleapis.com/v4/threatMatches:find?key= $googlekey');
    var googleResponse = await http.post(uri, body: jsonEncode({
      'client': {
        'clientId': 'qrsafe',
        'clientVersion': '1.0'
      },
      'threatInfo': {
        'threatTypes': ['MALWARE', 'SOCIAL_ENGINEERING', 'UNWANTED_SOFTWARE', 'POTENTIALLY_HARMFUL_APPLICATION', 'THREAT_TYPE_UNSPECIFIED'],
        'platformTypes': ['WINDOWS', 'ANDROID','ANY_PLATFORM'],
        'threatEntryTypes': ['URL', 'EXECUTABLE', 'THREAT_ENTRY_TYPE_UNSPECIFIED'],
        'threatEntries': [
          {'url': url}
        ]
      }
    }), headers: {'Content-Type': 'application/json'});
    if (googleResponse.statusCode == 200) 
    {
      final Map<String, dynamic> resp = jsonDecode(googleResponse.body);
      return resp;
    } 
    else 
    {
      throw Exception('Failed to load data');
    }

  }

   Future<Map<String,dynamic>> getVirusData(url) async
  {
    final Map<String, String> params = {
      'url': url
    };
    Uri uri = Uri.parse('https://www.virustotal.com/api/v3/urls').replace(queryParameters: params);
    http.Response absResponse = await http.post(uri,
    headers: {'Accept': 'application/json', 'x-apikey': viruskey, 'verbose': 'true'});
    
    if (absResponse.statusCode == 200) 
    {
      final Map<String, dynamic> resp = jsonDecode(absResponse.body);
      Uri uri = Uri.parse('https://www.virustotal.com/api/v3/analyses/${resp['data']['id']}');
      http.Response scanResponse = await http.get(uri, headers: {'Accept': 'application/json', 'x-apikey': viruskey});
      if (scanResponse.statusCode == 200) 
      {
         Map<String, dynamic> scanResp = jsonDecode(scanResponse.body);
        while (scanResp['data']['attributes']['status'] == 'queued') 
        {
          await Future.delayed(const Duration(seconds: 2));
          scanResponse = await http.get(uri, headers: {'Accept': 'application/json', 'x-apikey': viruskey});
          scanResp = jsonDecode(scanResponse.body);
        }
        return scanResp;
      } 
      else 
      {
        throw Exception('Failed to load data');
      }
    }
    else 
    {
      throw Exception('Failed to load data');
    }
  }