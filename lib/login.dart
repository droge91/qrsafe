

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'main.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key, required this.title});

  final String title;

  @override
  State<LoginPage> createState() => _LoginPageState();
}

final FirebaseAuth _auth = FirebaseAuth.instance;

class _LoginPageState extends State<LoginPage> 
{
    @override
    void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }
    final TextEditingController emailController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();

    void login() async
    {
      try 
      {
        final UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: emailController.text,
          password: passwordController.text,
        );
        emailController.clear();
        passwordController.clear();
        if (mounted)
        {
        FocusScope.of(context).unfocus();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const HomePage(title: 'QrSafe')),
        );
      }
      } 
      catch (e) 
      {
        if (mounted)
        {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('Error', style: TextStyle(color: Color.fromARGB(255, 0, 0, 0))),
              content: const Text("Username or password is incorrect", style: TextStyle(color: Color.fromARGB(255, 0, 0, 0))),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Close'),
                ),
              ],
            );
          },
        );
        }
      }
    }
    @override
    Widget build(BuildContext context) 
    {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          iconTheme: const IconThemeData(color: Colors.black),
          title: Text(widget.title, style: const TextStyle(color: Colors.black)),
          backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        ),
        body: 
        CustGradient(
          child: Center(
          child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              const Text('QR Safe', 
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold,color: Colors.black)),
              const SizedBox(height: 50,),
              Text(
                'Enter your email and password to login.',
                style: TextStyle(
                  fontStyle: FontStyle.values[0],
                  fontSize: 16,
                  color: const Color.fromARGB(255, 0, 0, 0),
                ),
              ),
              const SizedBox(height: 20,),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: const Color.fromARGB(255, 150, 150, 150)),
                  borderRadius: BorderRadius.circular(10),
                ),
                padding: const EdgeInsets.all(5),
                margin: const EdgeInsets.all(10),
                child:
              TextField(
                controller: emailController,
                style: const TextStyle(color: Colors.black),
                decoration: const InputDecoration(
                  hintStyle: TextStyle(color: Color.fromARGB(255, 150, 150, 150)),
                  hintText: 'Email',
                ),
              ),
              ),
              Container(
                 decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: const Color.fromARGB(255, 150, 150, 150)),
                  borderRadius: BorderRadius.circular(10),
                ),
                padding: const EdgeInsets.all(5),
                margin: const EdgeInsets.all(10),
                child:
              TextField(
                style: const TextStyle(color: Colors.black),
                obscureText: true,
                controller: passwordController,
                decoration: const InputDecoration(
                  hintStyle: TextStyle(color: Color.fromARGB(255, 150, 150, 150)),
                  hintText: 'Password',
                ),
              ),
              ),
              ElevatedButton(
                onPressed: ()
                { 
                  login();
                },
                child: const Text('Login'),
              ),
              ElevatedButton(
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const RegisterPage(title: 'Register')),
                  );
                },
                child: const Text('Register'),
              ),
            ],
          ),
        ),
      )
        )
      );
    }
}

class RegisterPage extends StatefulWidget 
{
  const RegisterPage({super.key, required this.title});

  final String title;

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage>
{
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController = TextEditingController();
  final auth = FirebaseAuth.instance;
  void login() async
  {
      try
      {
          await auth.signInWithEmailAndPassword(
          email: emailController.text,
          password: passwordController.text,
        );
      }
      catch(e)
      {
        //Firebase throws an exception here but still logs the user in as normal so leaving this for now
      }

  }

  void register() async 
  {
    try 
    {
      if (passwordController.text != confirmPasswordController.text) 
      {
        showDialog(
          context: context,
          builder: (context) {
            return
        AlertDialog(
          title: const Text('Error', style: TextStyle(color: Color.fromARGB(255, 0, 0, 0))),
          content: const Text('Passwords do not match', style: TextStyle(color: Color.fromARGB(255, 0, 0, 0))),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Close'),
            ),
          ],
        );
          },
        );
      }
      else
      {
      final UserCredential userCredential = await _auth.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );
      if (mounted)
      {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Success', style: TextStyle(color: Color.fromARGB(255, 0, 0, 0))),
            content: Text('User ${userCredential.user!.email} registered successfully. Would you like to login?', style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0))),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('Close'),
              ),
              TextButton(
                onPressed: () {
                login();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomePage(title: 'QrSafe')),
                );
              }, child: const Text('Login')
              ),
            ],
          );
        },
      );
      emailController.clear();
      passwordController.clear();
    }
    }
    }
    catch (e) 
    {
      if (mounted)
      {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Error', style: TextStyle(color: Color.fromARGB(255, 0, 0, 0))),
            content: Text(e.toString(), style: const TextStyle(color: Color.fromARGB(255,0,0,0)),),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('Close', style: TextStyle(color: Color.fromARGB(255, 0, 0, 0))),
              ),
            ],
          );
        },
      );
    }
    }
  }

  @override
  Widget build(BuildContext context) 
  {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        title: Text(widget.title, style: const TextStyle(color: Colors.black)),
      ),
      body: 
      CustGradient(
      child:
      Center(
        child:
        SingleChildScrollView(
          child:
           Column(
          children: <Widget>[
            const Text('QR Safe', 
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold,color: Colors.black)),
              const SizedBox(height: 50,),
              Text(
                'Enter your email and password to register.',
                style: TextStyle(
                  fontStyle: FontStyle.values[0],
                  fontSize: 16,
                  color: const Color.fromARGB(255, 0, 0, 0),
                ),
              ),
              const SizedBox(height: 20,),
            Container(
              margin: const EdgeInsets.all(10),
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: const Color.fromARGB(255, 150, 150, 150)),
                borderRadius: BorderRadius.circular(10),
              ),
              child:
            TextField(
              style: const TextStyle(color: Colors.black),
              controller: emailController,
              decoration: const InputDecoration(
                hintText: 'Email',
                hintStyle: TextStyle(color: Color.fromARGB(255, 150, 150, 150)),
              ),
            ),
            ),
              Container(
                 decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: const Color.fromARGB(255, 150, 150, 150)),
                  borderRadius: BorderRadius.circular(10),
                ),
                padding: const EdgeInsets.all(5),
                margin: const EdgeInsets.all(10),
              child:
            TextField(
              style: const TextStyle(color: Colors.black),
              controller: passwordController,
              decoration: const InputDecoration(
                hintText: 'Password',
                hintStyle: TextStyle(color: Color.fromARGB(255, 150, 150, 150)),
              ),
            ),
            ),
            Container(
                 decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: const Color.fromARGB(255, 150, 150, 150)),
                  borderRadius: BorderRadius.circular(10),
                ),
                padding: const EdgeInsets.all(5),
                margin: const EdgeInsets.all(10),
              child:
            TextField(
              style: const TextStyle(color: Colors.black),
              controller: confirmPasswordController,
              decoration: const InputDecoration(
                hintText: 'Confirm Password',
                hintStyle: TextStyle(color: Color.fromARGB(255, 150, 150, 150)),
              ),
              )
            ),
            ElevatedButton(
              onPressed: register,
              child: const Text('Register'),
            ),
          ],
        ),
      ),
      ),
      )
    );
  }
}