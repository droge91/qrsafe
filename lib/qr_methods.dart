
import 'main.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:drag_select_grid_view/drag_select_grid_view.dart';
import 'selectable_item.dart';
import 'selection_app_bar.dart';

final db = FirebaseFirestore.instance;

class QrList extends StatefulWidget 
{
  const QrList({super.key, required this.title});

  final String title;

  @override
  State<QrList> createState() => _QrListState();
}


Future<void> massDelete(List<String> selected) async 
{
    for (String i in selected) 
    {
      Query query = db.collection('qr_codes').where('file', isEqualTo: i);
      await query.get().then((snapshot) {
        final data = snapshot.docs.first.data() as Map<String, dynamic>;
        snapshot.docs.first.reference.delete();
        FirebaseStorage.instance.ref('images/${FirebaseAuth.instance.currentUser!.uid}/${data['file']}.png').delete();
      });
    }
}

Future<List<String>> getFilenamesfromIndexes(Set<int> indexes) async
{
  final user = FirebaseAuth.instance.currentUser!.uid;
  StorageService storageService = StorageService();
  final future = await storageService.listAllImages('images/$user');
  List<String> filenames = [];
  for (int i in indexes) 
  {
    filenames.add(future[i].key.substring(0, future[i].key.length - 4));
  }
  return filenames;
}

class _QrListState extends State<QrList> 
{
  final controller = DragSelectGridViewController();
  final user = FirebaseAuth.instance.currentUser!.uid;
  final GlobalKey<_galleryBodyState> galleryKey = GlobalKey<_galleryBodyState>();


  void scheduleRebuild()
  {
      setState(() {
      });
  }

  void refreshGallery() 
  {
    galleryKey.currentState!.rebuild();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: 
      SelectionAppBar(
        stateUpdater: refreshGallery,
        controller: controller,
        title: Text(widget.title),
      ),
      body: galleryBody(key: galleryKey,callback: scheduleRebuild, controller: controller),
    );
  }
}

class StorageService 
{
  final _storage = FirebaseStorage.instance;
  final user = FirebaseAuth.instance.currentUser;

   Future<List<MapEntry<String,String>>> listAllImages(String path) async
   {
    List<MapEntry<String,String>> imageUrls = [];
    try {
      final ListResult result = await _storage.ref(path).listAll();
      for (var ref in result.items) {
        final String downloadURL = await ref.getDownloadURL();
        imageUrls.add(MapEntry(ref.name, downloadURL));
      }
      return imageUrls;
    } catch (e) {
      throw Exception('Failed to list images: $e');
    }
  }
}

class QRPopup extends StatefulWidget 
{
  const QRPopup(this.query, this.url);
  final Query query;
  final String url;

  @override
  State<QRPopup> createState() => _QRPopupState();
}

class _QRPopupState extends State<QRPopup> 
{

  Future<QuerySnapshot> getQrData() async 
  {
    return await widget.query.get();
  }

  Future<void> deleteQr() async 
  {
    
    await widget.query.get().then((snapshot) {
      final data = snapshot.docs.first.data() as Map<String, dynamic>;
      snapshot.docs.first.reference.delete();
      FirebaseStorage.instance.ref('images/${FirebaseAuth.instance.currentUser!.uid}/${data['file']}.png').delete();
    });
  }

  @override
  Widget build(BuildContext context) 
  {
    return AlertDialog(
      backgroundColor: const Color.fromARGB(255, 255, 255, 255),
      titleTextStyle: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color:  Color.fromARGB(255, 0, 0, 0)),
      content:
      SingleChildScrollView(
        child:
      Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget> [
          FutureBuilder<QuerySnapshot>(
        future: getQrData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            if (snapshot.hasError)
            {
              return Center(child: Text('Error: ${snapshot.error}'));
            }
            return const Center(child: Text('No data found'));
          } else {
            final data = snapshot.data!.docs.first.data() as Map<String, dynamic>;
            return Column(
              children: [
                EditableText(data, snapshot, 'Title', style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color:  Color.fromARGB(255, 0, 0, 0))),
                Image.network(widget.url, width: 300, height: 300),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text('Endorsed: ', style: TextStyle(color: Colors.black),),
                EndorseSwitch(data, snapshot),
                  ]
                ),
                EditableText(data, snapshot, 'Description'),
              ],
            );
          }
        },
      ),
        ]
      ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Close'),
        ),
      ],
    );
  }
}

class EndorseSwitch  extends StatefulWidget 
{
  const EndorseSwitch(this.data, this.snapshot);
  final Map<String, dynamic> data;
  final AsyncSnapshot<QuerySnapshot> snapshot;


  @override
  State<EndorseSwitch> createState() => _EndorseSwitchState();
}

class _EndorseSwitchState extends State<EndorseSwitch> 
{
  final user = FirebaseAuth.instance.currentUser!.uid;
  final db = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) 
  {
     var data = widget.data;
     var snapshot = widget.snapshot;
    return Switch(
                  inactiveTrackColor: const Color.fromARGB(255, 0, 0, 0),
                  activeTrackColor: const Color.fromARGB(255, 86, 0, 136),
                  value: data['Trusted'],
                  onChanged: (value) {
                    db.collection('qr_codes').doc(snapshot.data!.docs.first.id).update({'Trusted': value});
                    setState(() {
                      data['Trusted'] = value;
              });
         },
      );
  }
}

class EditableText extends StatefulWidget 
{
  const EditableText(this.data, this.snapshot, this.text, {this.style = const TextStyle(fontSize: 20, color: Color.fromARGB(255, 0, 0, 0))});
  final Map<String, dynamic> data;
  final AsyncSnapshot<QuerySnapshot> snapshot;
  final TextStyle style;
  final String text;

  @override
  State<EditableText> createState() => _EditableTextState();
}

class _EditableTextState extends State<EditableText> 
{
  final db = FirebaseFirestore.instance;
  final TextEditingController _controller = TextEditingController();
  bool _isEditing = false;

  @override
  Widget build(BuildContext context) 
  {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('${widget.text}: ', style: widget.style),
        _isEditing 
        ?
         SizedBox(
          width: 150,
          child: TextField(
            controller: _controller,
            decoration: const InputDecoration(
              
              hintText: 'Enter new value',
            ),
            style: widget.style,
          ),
        )
        :
        Container(
        width: 150,
        child: Text(widget.data[widget.text], style: widget.style)
        ),
        _isEditing ?
         IconButton(
          icon: const Icon(Icons.save, color: Colors.black),
          onPressed: () {
            db.collection('qr_codes').doc(widget.snapshot.data!.docs.first.id).update({widget.text: _controller.text});
            setState(() {
              _isEditing = !_isEditing;
              widget.data[widget.text] = _controller.text;
            });
          },
        )
        : IconButton(
          icon: const Icon(Icons.edit, color: Colors.black),
          onPressed: () {
            setState(() {
              _isEditing = !_isEditing;
              _controller.text = widget.data[widget.text];
            });
          },
        )
      ],
      );
  }

}

class galleryBody extends StatefulWidget 
{
  galleryBody({key, required this.callback, required this.controller}) : super(key: key);
  final VoidCallback callback;

  final db = FirebaseFirestore.instance;

  final DragSelectGridViewController controller;

  @override
  State<galleryBody> createState() => _galleryBodyState();

}

class _galleryBodyState extends State<galleryBody>
{
  late Future<List<MapEntry<String,String>>> _future;

  final user = FirebaseAuth.instance.currentUser!.uid;
  final StorageService _storageService = StorageService();

  @override
  void initState() 
  {
    super.initState();
    _future = _storageService.listAllImages('images/$user');
  }

  void rebuild()
  {
    _future = _storageService.listAllImages('images/$user');
  }


  void scheduleRebuild() 
  {
    widget.callback();
  }
  @override
  Widget build(BuildContext context) 
  {
    widget.controller.addListener(scheduleRebuild);
     return CustGradient(
      child:
     FutureBuilder<List<MapEntry<String,String>>>(
        future: _future, 
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return const Center(child: Text('No images found'));
          } else {
             
            return DragSelectGridView(
              gridController: widget.controller,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 4.0,
                mainAxisSpacing: 4.0,
              ),
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index, selected) {
                String imageName = snapshot.data![index].key.substring(0, snapshot.data![index].key.length - 4);
                Query query = db.collection('qr_codes').where('file', isEqualTo: imageName);
                return 
                  SelectableItem(
                  index: index,
                  selected: selected,
                  image: Image.network(snapshot.data![index].value, fit: BoxFit.cover),
                  query: query,
                  url: snapshot.data![index].value,
                );
              },
            );
          }
        },
      )
     );
  }
}