import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mesh_gradient/mesh_gradient.dart';
import 'firebase_options.dart';
import 'dart:io';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'login.dart';
import 'qr_methods.dart';
import 'url_check.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:url_launcher/url_launcher.dart';
void main () async 
{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}
final db = FirebaseFirestore.instance;
class MyApp extends StatelessWidget 
{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context)
   {
    return MaterialApp(
      title: 'QRSafe',
      theme: ThemeData(

        colorScheme: const ColorScheme(
          brightness: Brightness.dark,
          primary: Color.fromARGB(255, 0, 0, 0),
          onPrimary: Color.fromARGB(255, 252, 252, 252),
          secondary: Colors.red,
          onSecondary: Colors.white,
          error: Colors.red,
          onError: Color.fromARGB(255, 0, 0, 0),
          surface: Color.fromARGB(255, 255, 255, 255),
          onSurface: Color.fromARGB(255, 255, 255, 255),
          ),
        primaryColor: Colors.black,
        focusColor: const Color.fromARGB(255, 0, 0, 0),

        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: TextTheme(
          displayLarge: const TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
          titleLarge: GoogleFonts.oswald(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
          bodyMedium: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.normal),
          displaySmall: GoogleFonts.oswald(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        useMaterial3: true,
      ),
      routes: {
      '/list': (context) => const QrList(title: 'Gallery'),
      '/login': (context) => const LoginPage(title: 'Login'),
      },
      home: const HomePage(title: 'QRSafe'),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});



  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> 
{
  BarcodeCapture? qr;
  Image? image;



  Future<void> _pickAndUploadImage() async 
  {
    MobileScannerController controller = MobileScannerController();
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.camera);
    late final String user;
    if (FirebaseAuth.instance.currentUser == null) 
    {
      user = 'Anonymous';
    }
    else
    {
     user = FirebaseAuth.instance.currentUser!.uid;
    }

    if (pickedFile != null) 
    {
      final File imageFile = File(pickedFile.path);
      final String fileName = DateTime.now().millisecondsSinceEpoch.toString();
      final Reference storageRef = FirebaseStorage.instance.ref().child('images/$user/$fileName.png');
      var timestamp = DateTime.now().toString();
      qr = await controller.analyzeImage(imageFile.path);
      if (qr == null) 
      {
        if (mounted)
        {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('No QR code found', style: TextStyle(color: Colors.black)),
              actions: <Widget>[
                TextButton(
                  onPressed: () 
                  {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Close'),
                ),
              ],
            );
          }
        );
        return;
        }
      }
      db.collection('qr_codes').doc(timestamp).set({
        'user': user,
        'file': fileName,
        'timestamp': DateTime.now(),
        'Title': 'QR Code',
        'Description' : 'QR Code',
        'Trusted': false,
        'bytes': qr!.barcodes[0].rawBytes,
      });
      final UploadTask uploadTask = storageRef.putFile(imageFile);
      await uploadTask.whenComplete(() => null);
      
      if (qr != null)
      {
          setState(() {
            image = Image.file(imageFile);
          });

      }
    }
  }
  
  Future<void> _pickFromGallery() async 
  {
    MobileScannerController controller = MobileScannerController();
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    late final String user;
    if (FirebaseAuth.instance.currentUser == null) 
    {
      user = 'Anonymous';
    }
    else
    {
     user = FirebaseAuth.instance.currentUser!.uid;
    }

    if (pickedFile != null) 
    {
      final File imageFile = File(pickedFile.path);
      final String fileName = DateTime.now().millisecondsSinceEpoch.toString();
      final Reference storageRef = FirebaseStorage.instance.ref().child('images/$user/$fileName.png');
      var timestamp = DateTime.now().toString();
      qr = await controller.analyzeImage(imageFile.path);
      if (qr == null) 
      {
        if (mounted)
        {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('No QR code found', style: TextStyle(color: Colors.black)),
              actions: <Widget>[
                TextButton(
                  onPressed: () 
                  {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Close'),
                ),
              ],
            );
          }
        );
        return;
        }
      }
      db.collection('qr_codes').doc(timestamp).set({
        'user': user,
        'file': fileName,
        'timestamp': DateTime.now(),
        'Title': 'QR Code',
        'Description' : 'QR Code',
        'Trusted': false,
        'bytes': qr!.barcodes[0].rawBytes,
      });
      final UploadTask uploadTask = storageRef.putFile(imageFile);
      await uploadTask.whenComplete(() => null);
      
      if (qr != null)
      {
          setState(() {
            image = Image.file(imageFile);
          });

      }
    }
  }



  @override
  Widget build(BuildContext context) 
  {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Color.fromARGB(255, 0, 0, 0)),
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        centerTitle: true,
        title: const Text('QRSafe', style: TextStyle(color: Color.fromARGB(255, 0, 0, 0))),

      ),
      drawer: const CustDrawer(),
      body:
      CustGradient(
      child:
          Stack(
            children: [
        Center(
          child:
          Column(
            children: [
            qr == null 
            ?
            const QRHomeDisplay()
            :
            QRResultsDisplay(qr: qr!, image: image!),
            const SizedBox(
              height: 20,

            ),
            Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromARGB(255, 0, 0, 0),
                      spreadRadius: 1.0,
                      offset: Offset(0, 0),
                    )
                  ]
                ),
                child:
              IconButton(
                iconSize: 30,
              onPressed: _pickAndUploadImage,
              icon: const Icon(Icons.camera_alt),
              ),  
              ),
              const SizedBox(
                width: 50,
              ),
              Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromARGB(255, 0, 0, 0),
                      spreadRadius: 1.0,
                      offset: Offset(0, 0),
                    )
                  ]
                ),
                child:
              IconButton(
                iconSize: 30,
              onPressed: _pickFromGallery,
              icon: const Icon(Icons.folder_open),
              ),
              ),
            ]
            ),
        ],
      )
        ),
        Align(
              alignment: Alignment.bottomRight,
              child: 
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromARGB(255, 0, 0, 0),
                      spreadRadius: 1.0,
                      offset: Offset(0, 0),
                    )
                  ]
                ),
                margin: const EdgeInsets.all(10),

              child:
            IconButton(
            onPressed: () 
            {
              if (FirebaseAuth.instance.currentUser != null) 
              {
                Navigator.pushNamed(context, '/list');
              }
              else
              {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: const Text('Please login to view your gallery', style: TextStyle(color: Colors.black))  ,
                      actions: <Widget>[
                        TextButton(
                          onPressed: () 
                          {
                            Navigator.of(context).pop();
                          },
                          child: const Text('Close'),
                        ),
                      ],
                    );
                  }
                );
              
              }
            },
            icon: const Icon(Icons.photo_library),
            ),
              )
            ),
            ]
    )
       )
      );
  }
}

class QRHomeDisplay extends StatefulWidget 
{
  const QRHomeDisplay({super.key});


  @override
  State<QRHomeDisplay> createState() => _QRHomeDisplayState();
}

class _QRHomeDisplayState extends State<QRHomeDisplay> 
{
  
  String getType(String value) 
  {
    final Uri? uri = Uri.tryParse(value);
    if (uri != null && uri.hasScheme && uri.hasAuthority) 
    {
      return "URL";
    }
    return "Text";
  }

  String getHost(String value) 
  {
    final Uri? uri = Uri.tryParse(value);
    if (uri != null && uri.hasScheme && uri.hasAuthority) 
    {
      return uri.host;
    }
    return "";
  }

  getWelcome() async
  {
    final storage = FirebaseStorage.instance;
    Reference welcomeRef = storage.ref('images/Stock/').child('welcome.png');
    String welcomeURl = await welcomeRef.getDownloadURL();
    return Image.network(welcomeURl);
  }

  @override
  Widget build(BuildContext context) 
  {
     return Column(
          children: <Widget>[
              const SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: const Color.fromARGB(255, 5, 0, 46),
                    width: 7,
                    ),
                ),
              width: 180,
              height: 180,
              child:  FutureBuilder(
                future: getWelcome(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    return snapshot.data as Widget;
                  }
                },
              )
            ),
            const SizedBox(height: 10),
           Container(
            decoration: BoxDecoration(
             
              borderRadius: BorderRadius.circular(20),
              border: Border.all(
                color: const Color.fromARGB(255, 20, 7, 80),
                
                width: 7,
                style: BorderStyle.solid,
                ),
                color: Colors.white,
            ),
            width: 320,
            height: 400,
            child:
            ListView(
              children: const <Widget>[
                ListTile(
                  title: Text('Type', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  subtitle: Text('Text', style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.black)),
                  ),
                ListTile(
                  title: Text('Value', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  subtitle: Text('Welcome to QRSafe', style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.black)),
                ),
              ],
            ),
           )
          ]
    );
  }
}

class QRResultsDisplay extends StatefulWidget 
{
  const QRResultsDisplay({super.key, required this.qr, required this.image});

  final BarcodeCapture qr;

  final Image image;

  @override
  State<QRResultsDisplay> createState() => _QRResultsDisplayState();
}

class _QRResultsDisplayState extends State<QRResultsDisplay> 
{
  String getType(String value) 
  {
    final Uri? uri = Uri.tryParse(value);
    if (uri != null && uri.hasScheme && uri.hasAuthority) 
    {
      return "URL";
    }
    return "Text";
  }
 

  late Future<Map<String,dynamic>> virusData;
  late Future<Map<String,dynamic>> apivoidData;
  late Future<Map<String,dynamic>> googleData;
  late String? url;
  late String type;
  @override
  void initState()
  {
    type = getType(widget.qr.barcodes[0].rawValue ?? '');
    super.initState();
    if (type == 'URL') 
    {
      url = widget.qr.barcodes[0].rawValue;
      virusData = getVirusData(url);
      apivoidData = getAPIVoidData(url);
      googleData = getGoogleData(url);
    }
  }

  Future<String> getRating(dataBaseInstaces) async 
  {
    int rating = 100;
    final voidData = await apivoidData;
    final virus = await virusData;
    final google = await googleData;
    final voidchecks = voidData['data']['report']['domain_blacklist']['engines'];
    final voidrating = voidData['data']['report']['risk_score']['result'];
    final viruschecks = virus['data']['attributes']['results'];
    
    if (dataBaseInstaces != Null)
    {
      final dataBaseInstances = await dataBaseInstaces;
      for (var doc in dataBaseInstances.docs)
      {
        if (doc['Trusted'] == true)
        {
          rating += 10;
        }
      }
    }
    if (google.isNotEmpty)
    {
      final googlechecks = google['matches'];
      rating -= googlechecks.length * 10 as int;
    }
    
    for (var check in voidchecks) 
    {
      if (check['detected'] == 1) 
      {
        rating -= 10;
      }
    }
    
    final virusflagcount = viruschecks.entries.
            where((entry) => entry.value['result'] == 'phishing' || entry.value['result'] == 'malicious').length;
    rating -= (virusflagcount as int) * 10;
    rating -= (voidrating as int);
    switch (rating) 
    {
      case 100:
        return 'Safe';
      case 90:
        return 'Low Risk';
      case 80:
        return 'Moderate Risk';
      case 70:
        return 'High Risk';
      case 60:
        return 'Dangerous';
      case 50:
        return 'Malicious';
      default:
        return 'Malicious';
    }
  }

  @override
  Widget build(BuildContext context) 
  {
    type = getType(widget.qr.barcodes[0].rawValue ?? '');
    if (type == 'URL') 
    {
      url = widget.qr.barcodes[0].rawValue;
      virusData = getVirusData(url);
      apivoidData = getAPIVoidData(url);
      googleData = getGoogleData(url);
    }
      Query query = db.collection('qr_codes').where('bytes', isEqualTo: widget.qr.barcodes[0].rawBytes).where('timestamp', isLessThan: DateTime.now().subtract(const Duration(minutes: 5)));
      final dataBaseInstances = query.get();
      return Column(
          children: <Widget>[
              const SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: const Color.fromARGB(255, 0, 0, 0),
                    width: 7,
                    ),
                    image: DecorationImage(
                      image: widget.image.image,
                      fit: BoxFit.cover,
                ),
                ),
              width: 180,
              height: 180, 
              ),
            const SizedBox(height: 10),
           Container(
            decoration: BoxDecoration(
              
              borderRadius: BorderRadius.circular(20),
              border: Border.all(
                color: const Color.fromARGB(255, 0, 0, 0),
                
                width: 7,
                style: BorderStyle.solid,
                ),
                color: Colors.white,
            ),
            width: 320,
            height: 400,
            child:
            ListView(
              children: <Widget>[
                ListTile(
                  title: const Text('Type', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  subtitle: Text(type, style: const TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.black),
                  ),
                ),
                type == 'Text'
                ?
                ListTile(
                  title: const Text('Value', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  subtitle: Text(widget.qr.barcodes[0].rawValue ?? '', style: const TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.black),
                  ),
                )
                :
                Column(
                  children: <Widget>[
                ListTile(
                  title: const Text('Value', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  subtitle: Text(widget.qr.barcodes[0].rawValue ?? '', style: const TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.black),
                  )),
                ListTile(
                  title: const Text('Safety Rating', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  subtitle: Row(
                    children: [FutureBuilder(
                    future: getRating(dataBaseInstances),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const CircularProgressIndicator();
                      } else if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else {
                        return Text(snapshot.data as String, style: const TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.black));
                      }
                    },
                  ),
                  const SizedBox(width: 10,),
                  const Tooltip(
                    message: 'Safety rating is based on the results of multiple URL scanning services',
                    child:Icon(Icons.info, color: Colors.black),
                  )
                    ],
                ),
                ),
                ListTile(
                  title: const Text('Database Matches', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  subtitle: 
                  FutureBuilder(
                    future: dataBaseInstances,
                    builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const CircularProgressIndicator();
                      } else if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else {
                        int length = snapshot.data!.docs.length;
                        int trusts = 0;
                        for (var doc in snapshot.data!.docs)
                        {
                            if (doc['Trusted'] == true)
                            {
                              trusts += 1;
                            }
  
                        }
                        return Text('Matches found: $length, Endorsements $trusts', style: const TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.black));
                    }
                    },
                  )
                ),
                TextButton(
                  onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return URLDetails(virusData: virusData, apivoidData: apivoidData, googleData: googleData);
                    }
                  );
                  },
                  child: const Text('URL Details', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 74, 44, 207))),
                  ),
                  TextButton(
                  onPressed: () {
                    launchUrl(Uri.parse(widget.qr.barcodes[0].rawValue ?? ''),);
                  },
                  child: const Text('Visit URL', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 74, 44, 207))),
                  ),

                  ],
                  )
                  ],
                  ),
           )
              ]
            );
  }
}

class CustGradient extends StatelessWidget {
  const CustGradient({required this.child, super.key});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MeshGradient(
        options: MeshGradientOptions(
          blend: 2.5,
          noiseIntensity: 0.2,
        ),
        points: [
        MeshGradientPoint(
          position: const Offset(
            0,
            0,
          ),
          color: const Color.fromARGB(255, 255, 255, 255),
        ),
        MeshGradientPoint(
          position: const Offset(
            1,
            0,
          ),
          color: const Color.fromARGB(255, 255, 255, 255),
        ),
        MeshGradientPoint(
          position: const Offset(
            0,
            1,
          ),
          color: const Color.fromARGB(255, 25, 0, 116),
        ),
        MeshGradientPoint(
          position: const Offset(
            1,
            1,
          ),
          color: const Color.fromARGB(0, 28, 8, 109),
        ),
      ],
      child: child,
    );
  }
}


class CustDrawer extends Drawer {
  const CustDrawer({required ,super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child:
      CustGradient(
        child:
      ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const SizedBox(height: 80,),
          
            FirebaseAuth.instance.currentUser == null
            ?
            const Center(child:  Text('Welcome to QRSafe', style: TextStyle(color: Colors.black, fontSize: 20)))
            :
            Center(child: Text('Welcome, ${FirebaseAuth.instance.currentUser!.email}', style: const TextStyle(color: Colors.black, fontSize: 20))),
          const SizedBox(height: 70,),
          FirebaseAuth.instance.currentUser == null
          ?
          ListTile(
            title: const Row(
              children: [
                Icon(Icons.person, color: Colors.black,),
                SizedBox(width: 10,),
                Text('Sign up/Login', style: TextStyle(color: Colors.black)),
              ]
            ),
            onTap: () {
              Navigator.pushNamed(context, '/login');
            },
          )
          :
          ListTile(
            title:  const Row(
              children: [
                Icon(Icons.logout, color: Colors.black,),
                SizedBox(width: 10,),
                Text('Logout', style: TextStyle(color: Colors.black)),
              ]
            ),
            onTap: () async {
              await FirebaseAuth.instance.signOut();
              Navigator.pop(context);
            },
          ),
        ],
      ),
    )
    );
  }
}


